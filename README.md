Aim: 
To make a cloud-app using AWS or Azure to help IT industries maintain/catalog inventory for common issues which they face.
-----------------------------------------------------------------------------------------------------------------------------
Idea: 
IT-Industries develop new codes/APIs on day-to-day basis, they also deploy them in systematic timelines.
Many of the times, there are some common issues faced like DB connectivity issues, Revoked access issues.
Instead of searching solutions on internet, teams can beforehand catalog issues and their corresponding
solutions, which can be searched easily during times of critical deployments.
-----------------------------------------------------------------------------------------------------------------------------
Scope: 
1. Proper user login and access control.
2. Interface to log errors and their resolutions.
3. Service to upload files like checklist, screenshots, guides etc.
4. Search service to easily find debugging strategies and content.
-----------------------------------------------------------------------------------------------------------------------------

Techstack:

Collaboration
1. Microsoft Teams
2. JIRA
3. Bitbucket
4. MS Teams Bitbucket Connector